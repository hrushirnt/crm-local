'use strict';
//  const {SendNotifiaction}=require('./functions.js');
const express = require('express');
const catalyst = require("zcatalyst-sdk-node");
const fs = require('fs');
const axios=require('axios');
const https= require("https");
const FormData = require('form-data');
const cheerio = require('cheerio');
const path=require('path');
const Readable = require('stream').Readable
const mysql2= require("mysql2");
const app = express();
app.use(express.json());

const connection=mysql2.createPool({
	host:"localhost",
	port:3306,
	user:"root",
	password:"root",
	database:"zoho_catalyst_local"
})
connection.getConnection((err)=>{
	if(!err){
		console.log("database connectred successfully!");
	}else{
		console.log("Error while connecting : ",err);
	}
})

const httpsAgent = new https.Agent({
  rejectUnauthorized: false,
});

// Tidal Image download
app.get('/download', async (req,res) => {
	try{
		async function downloadImageToBuffer(url) {
		const response = await axios({
			url,
			method: 'GET',
			responseType: 'arraybuffer'
		});
		const buffer = Buffer.from(response.data, 'binary');
		res.status(200).json({
			"success" : true,
			"data" : buffer.toString('base64')
		})
		}
		downloadImageToBuffer('https://map1.msc.fema.gov/api/firm/3604970219F');
	}
	catch(error){
		res.status(404).json({
			"success" : false,
			"message" : error.message
		})
	}
})

// Database APIs START HERE
// getting data from data table using bbl
app.get('/getCoordinates', (req, res) => {
	try{
		const query = `SELECT ID,BBL,Coordinates FROM Tax_Lot_Polygons WHERE BBL = ${req.query.bbl};`;
		const appData = catalyst.initialize(req);
		const zcql = appData.zcql();
		const zcqlPromise = zcql.executeZCQLQuery(query);
		zcqlPromise.then(queryResult => {
			res.status(200).json({ 
				"success" : true,
				"data" : queryResult[0].Tax_Lot_Polygons,
			})
		})
		.catch((error) => {
			res.status(500).json({ 
				"success" : false,
				"data" : "Please, provide a valid BBL number."
			})
		})
	}
	catch(error){
		res.status(404).json({ 
				"success" : false,
				"data" : error.message
			})
	}
})

// getting data from data table
app.get('/getOptionsList/:DB', (req, res) => {
	try{
		const query = `SELECT * FROM ${req.params['DB']}`;
		const appData = catalyst.initialize(req);
		const zcql = appData.zcql();
		const zcqlPromise = zcql.executeZCQLQuery(query);
		zcqlPromise.then(queryResult => {
			res.status(200).json({ 
				"success" : true,
				"data" : queryResult,
				"records" : queryResult.length
			})
		})
		.catch((error) => {
			res.status(500).json({ 
				"success" : false,
				"data" : "Please, provide a valid Database path"
			})
		})
	}
	catch(error){
		res.status(404).json({ 
				"success" : false,
				"data" : error.message
			})
	}
})

// getting data from data table
app.post('/insertDataInDB/:DB', (req, res) => {
	try{
		const query = `INSERT INTO ${req.params['DB']} (BBL,Coordinates) VALUES ('${req.body.bbl}','${req.body.cord}');`
		const appData = catalyst.initialize(req);
		const zcql = appData.zcql();
		const zcqlPromise = zcql.executeZCQLQuery(query);
		zcqlPromise.then(queryResult => {
			res.status(200).json({ 
				"success" : true,
				"data" : "Data added successfully."
			})
		})
		.catch((error) => {
			res.status(500).json({ 
				"success" : false,
				"data" : "Please, provide a valid Database path"
			})
		})
	}
	catch(error){
		res.status(404).json({ 
				"success" : false,
				"data" : error.message
			})
	}
})

// getting data from data table
app.post('/insertDataInBulk/:DB', (req, res) => {
	try{
		const query = `INSERT INTO ${req.params['DB']} (ID,BBL,Coordinates) VALUES ${req.body.bulkQuery};`
		const appData = catalyst.initialize(req);
		const zcql = appData.zcql();
		const zcqlPromise = zcql.executeZCQLQuery(query);
		zcqlPromise.then(queryResult => {
			res.status(200).json({ 
				"success" : true,
				"data" : "Data added successfully."
			})
		})
		.catch((error) => {
			res.status(500).json({ 
				"success" : false,
				"message" : error.message
			})
		})
	}
	catch(error){
		res.status(404).json({ 
				"success" : false,
				"data" : error.message
			})
	}
})

// set Api_WebScraping_Monitoring data
app.post('/postApiAndWebScrapingMonitoring', (req, res) => {
	try{
		const query = `INSERT INTO Api_WebScraping_Monitoring VALUES (${req.body.BBL_Number},${req.body.Deals_ID},${req.body.Tidal_Wetlands_API},${req.body.Tidal_Wetlands_WebScraping},${req.body.Tidal_Wetlands_Map_Index_Number},${req.body.Tidal_Wetlands_Category},${req.body.Tidal_Wetlands_Image_URL},${req.body.Effective_Flood_Hazard_Checkzones},${req.body.Effective_BFE},${req.body.Preliminary_Flood_Hazard_Checkzones},${req.body.Preliminary_BFE},${req.body.Collective_Result_Flood_Hazard_Checkzones},${req.body.Flood_Hazard_Zones_WebScarping_API},'${req.body.Effective_Firm_Panel_Number}','${req.body.Preliminary_Firm_Panel_Number}',${req.body.State_Regulated_Freshwater_Wetlands},${req.body.State_Regulated_Freshwater_Wetlands_Checkzone},${req.body.Freshwater_Wetlands_WebScraping},${req.body.Environmental_Restrictions_API},${req.body.Environmental_Restrictions_WebScraping},${req.body.Landmark_API},${req.body.Landmark_WebScraping},${req.body.Coastal_Erosion_Hazard_Area_API},${req.body.Coastal_Erosion_Hazard_Area_WebScraping});`
		const appData = catalyst.initialize(req);
		const zcql = appData.zcql();
		const zcqlPromise = zcql.executeZCQLQuery(query);
		zcqlPromise.then(queryResult => {
			res.status(200).json({ 
				"success" : true,
				"data" : "Data added successfully.",
			})
		})
		.catch((error) => {
			res.status(500).json({ 
				"success" : false,
				"data" : error
			})
		})
	}
	catch(error){
		res.status(404).json({ 
				"success" : false,
				"data" : error
			})
	}
})

// DATABASE APIs END HERE

// NYC APIs START HERE
// gets details of the nyc property info
app.get('/getNYCPropertyInfo', async (req, res) => {
	const { Borough, AddressNo, StreetName } = req.query

	if(Borough && AddressNo && StreetName){
		try{
			const resData = await axios.get(`https://geoservice.planning.nyc.gov/geoservice/geoservice.svc/Function_1A?Borough=${Borough}&AddressNo=${AddressNo}&StreetName=${StreetName}&Key=4t7wXzBCHF7JaNdR`,{ httpsAgent });
			if(!resData.data.display.out_error_message.trim()){
				res.status(200).json({ 
									"success" : true,
									"bin" : resData.data.display.out_bin , 
									"bbl" : resData.data.display.out_bbl , 
									"data" : resData.data
								});
			} else {
				res.status(200).json({ 
									"success" : false,
									"message" : resData.data.display.out_error_message.trim()
								});
			}
			}
		catch(error){
			res.status(404).json({
				"success" : false,
				"message" : error.message,
			})
		}
	} else {
		res.status(404).json({
				"success" : false,
				"message" : "Parameter missing or Invalid ",
			})
	}
});

// get info from PLUTO API
app.get('/getPLUTOInfo', async (req, res) => {
	if(req.query.bbl){
		try{
			const resData = await axios.get(`https://data.cityofnewyork.us/resource/64uk-42ks.json?bbl=${req.query.bbl}`);
			res.status(200).json({ "success" : true, 'data' : resData.data});
		}
		catch(error){
			res.status(500).json({
				"success" : false,
				"message" : error.message,
			})
		}
	} else {
		res.status(204).send({});
	}
});

// get Zoning Tax Info
app.get('/getZoningTaxInfo', async (req, res) => {
	if(req.query.bbl){
		try{
			const resData = await axios.get(`https://data.cityofnewyork.us/resource/fdkv-4t4z.json?bbl=${req.query.bbl}`)
			res.status(200).json({
				"success" : true,
				"data" : resData.data
			});
		}
		catch(error){
			res.status(500).json({
				"success" : false,
				"message" : error.message,
			})
		}
	} else {
		res.status(204).send({});
	}
});

// Environmental Restrictions API
app.get('/getEnvResInfo', async (req, res) => {
	if(req.query.bbl){
		try{
			const resData = await axios.get(`https://data.cityofnewyork.us/resource/jsrs-ggnx.json?bbl=${req.query.bbl}`)
			res.status(200).json({
				"success" : true,
				"data" : resData.data
			});
		}
		catch(error){
			res.status(500).json({
				"success" : false,
				"message" : error.message,
			})
		}
	} else {
		res.status(204).send({});
	}
});

// get Zoning Tax Info
app.get('/getLandmarkInfo', async (req, res) => {
	if(req.query.bbl){
		try{
			const resData = await axios.get(`https://data.cityofnewyork.us/resource/ncre-qhxs.json?bbl=${req.query.bbl}`)
			res.status(200).json({
				"success" : true,
				"data" : resData.data
			});
		}
		catch(error){
			res.status(500).json({
				"success" : false,
				"message" : error.message,
			})
		}
	} else {
		res.status(204).send({});
	}
});

// get Flood Zone both eff and prelim
app.post('/getFloodZoneInfo', async (req, res) => {
	if(req.body.coordinates){
		try{
			const encodeCoordinates = encodeURI(req.body.coordinates) 
			const effFloodZoneData = await axios.get(`https://hazards.fema.gov/gis/nfhl/rest/services/public/NFHL/MapServer/28/query?where=1%3D1&geometry=${encodeCoordinates}&geometryType=esriGeometryPolygon&inSR=2263&spatialRel=esriSpatialRelIntersects&units=esriSRUnit_Foot&outFields=*&returnGeometry=false&returnTrueCurves=false&returnIdsOnly=false&returnCountOnly=false&returnZ=false&returnM=false&returnDistinctValues=false&returnExtentOnly=false&featureEncoding=esriDefault&f=json`);
			const prelimFloodZoneData = await axios.get(`https://hazards.fema.gov/gis/nfhl/rest/services/PrelimPending/Prelim_NFHL/MapServer/24/query?where=1%3D1&geometry=${encodeCoordinates}&geometryType=esriGeometryPolygon&inSR=2263&spatialRel=esriSpatialRelIntersects&units=esriSRUnit_Foot&outFields=*&returnGeometry=false&returnTrueCurves=false&returnIdsOnly=false&returnCountOnly=false&returnZ=false&returnM=false&returnDistinctValues=false&returnExtentOnly=false&featureEncoding=esriDefault&f=json`);
			res.status(200).json({
				"success" : true,
				"data" : {
					"EffectiveFloodZone" : effFloodZoneData.data.features ? {
						"DFIRM_ID" : effFloodZoneData.data.features[0].attributes.DFIRM_ID,
						"FLD_ZONE" : effFloodZoneData.data.features[0].attributes.FLD_ZONE,
						"STATIC_BFE" : effFloodZoneData.data.features[0].attributes.STATIC_BFE,
						"V_DATUM" : effFloodZoneData.data.features[0].attributes.V_DATUM,
						"ZONE_SUBTY" : effFloodZoneData.data.features[0].attributes.ZONE_SUBTY,
					} : null,
					"PreliminaryFloodZone" :  prelimFloodZoneData.data.features ? {
						"DFIRM_ID" : prelimFloodZoneData.data.features[0].attributes.DFIRM_ID,
						"FLD_ZONE" : prelimFloodZoneData.data.features[0].attributes.FLD_ZONE,
						"STATIC_BFE" : prelimFloodZoneData.data.features[0].attributes.STATIC_BFE,
						"V_DATUM" : prelimFloodZoneData.data.features[0].attributes.V_DATUM,
						"ZONE_SUBTY" : prelimFloodZoneData.data.features[0].attributes.ZONE_SUBTY,
					} : null
				}
			});
		}
		catch(error){
			res.status(500).json({
				"success" : false,
				"message" : error.message,
			})
		}
	} else {
		res.status(204).send({});
	}
});

// get Firm Panel both eff and prelim
app.post('/getFirmPanelInfo', async (req, res) => {
	if(req.body.coordinates){
		try{
			const encodeCoordinates = encodeURI(req.body.coordinates) 
			const effFirmPanelData = await axios.get(`https://hazards.fema.gov/gis/nfhl/rest/services/CNMS/basedata/MapServer/6/query?where=1%3D1&geometry=${encodeCoordinates}&geometryType=esriGeometryPolygon&inSR=2263&spatialRel=esriSpatialRelIntersects&units=esriSRUnit_Foot&outFields=*&returnGeometry=false&returnTrueCurves=false&returnIdsOnly=false&returnCountOnly=false&returnZ=false&returnM=false&returnDistinctValues=false&returnExtentOnly=false&featureEncoding=esriDefault&f=json`);
			const prelimFirmPanelData = await axios.get(`https://hazards.fema.gov/gis/nfhl/rest/services/PrelimPending/Prelim_NFHL/MapServer/1/query?where=1%3D1&geometry=${encodeCoordinates}&geometryType=esriGeometryPolygon&inSR=2263&spatialRel=esriSpatialRelIntersects&units=esriSRUnit_Foot&outFields=*&returnGeometry=false&returnTrueCurves=false&returnIdsOnly=false&returnCountOnly=false&returnZ=false&returnM=false&returnDistinctValues=false&returnExtentOnly=false&featureEncoding=esriDefault&f=json`);
			res.status(200).json({
				"success" : true,
				"data" : {
					"EffectiveFirmPanelNo" : effFirmPanelData.data.features ? effFirmPanelData.data.features[0].attributes.FIRM_PAN : null,
					"PreliminaryFirmPanelNo" :  prelimFirmPanelData.data.features ? prelimFirmPanelData.data.features[0].attributes.FIRM_PAN : null
				}
			});
		}
		catch(error){
			res.status(500).json({
				"success" : false,
				"message" : error.message,
			})
		}
	} else {
		res.status(204).send({});
	}
});

// get Tidal wetlands info
app.post('/getTidalWetlandsInfo', async (req, res) => {
	if(req.body.coordinates){
		try{
			const encodeCoordinates = encodeURI(req.body.coordinates) 
			const tidalWetlandsData = await axios.get(`https://services6.arcgis.com/DZHaqZm9cxOD4CWM/ArcGIS/rest/services/Regulatory_Tidal_Wetlands/FeatureServer/1/query?where=1%3D1&geometry=${encodeCoordinates}&geometryType=esriGeometryPolygon&inSR=2263&spatialRel=esriSpatialRelIntersects&resultType=none&distance=300&units=esriSRUnit_Foot&returnGeodetic=false&outFields=*&returnGeometry=false&returnCentroid=false&featureEncoding=esriDefault&multipatchOption=xyFootprint&applyVCSProjection=false&returnIdsOnly=false&returnUniqueIdsOnly=false&returnCountOnly=false&returnExtentOnly=false&returnQueryGeometry=false&returnDistinctValues=false&cacheHint=false&returnZ=false&returnM=false&returnExceededLimitFeatures=false&sqlFormat=none&f=json`);
			res.status(200).json({
				"success" : true,
				"data" : tidalWetlandsData.data.features,
				"IMAGEURL" : tidalWetlandsData.data.features[0] ? tidalWetlandsData.data.features[0].attributes.IMAGEURL : null,
				"MAPINDEX" : tidalWetlandsData.data.features[0] ? tidalWetlandsData.data.features[0].attributes.MAPINDEX : null
			});
		}
		catch(error){
			res.status(500).json({
				"success" : false,
				"message" : error.message,
			})
		}
	} else {
		res.status(204).send({});
	}
});

// get Freshwater wetlands info
app.post('/getFreshwaterWetlandsInfo', async (req, res) => {
	if(req.body.coordinates){
		try{
			const encodeCoordinates = encodeURI(req.body.coordinates) 
			const freshwaterWetlandsData = await axios.get(`https://services6.arcgis.com/DZHaqZm9cxOD4CWM/ArcGIS/rest/services/State_Regulated_Freshwater_Wetlands/FeatureServer/0/query?where=1%3D1&geometry=${encodeCoordinates}&geometryType=esriGeometryPolygon&inSR=2263&spatialRel=esriSpatialRelIntersects&resultType=none&units=esriSRUnit_Foot&returnGeodetic=false&outFields=*&returnGeometry=false&returnCentroid=false&featureEncoding=esriDefault&multipatchOption=xyFootprint&applyVCSProjection=false&returnIdsOnly=false&returnUniqueIdsOnly=false&returnCountOnly=false&returnExtentOnly=false&returnQueryGeometry=false&returnDistinctValues=false&cacheHint=false&returnZ=false&returnM=false&returnExceededLimitFeatures=false&sqlFormat=none&f=json`);
			const freshwaterCheckzoneData = await axios.get(`https://services6.arcgis.com/DZHaqZm9cxOD4CWM/ArcGIS/rest/services/State_Regulated_Freshwater_Wetlands/FeatureServer/1/query?where=1%3D1&geometry=${encodeCoordinates}&geometryType=esriGeometryPolygon&inSR=2263&spatialRel=esriSpatialRelIntersects&resultType=none&units=esriSRUnit_Foot&returnGeodetic=false&outFields=*&returnGeometry=false&returnCentroid=false&featureEncoding=esriDefault&multipatchOption=xyFootprint&applyVCSProjection=false&returnIdsOnly=false&returnUniqueIdsOnly=false&returnCountOnly=false&returnExtentOnly=false&returnQueryGeometry=false&returnDistinctValues=false&cacheHint=false&returnZ=false&returnM=false&returnExceededLimitFeatures=false&sqlFormat=none&f=json`);
			res.status(200).json({
				"success" : true,
				"data" : {
					"FreshwaterWetlands" : freshwaterWetlandsData.data.features[0] ? freshwaterWetlandsData.data.features[0].attributes : null,
					"FreshwaterCheckzone" : freshwaterCheckzoneData.data.features[0] ? freshwaterCheckzoneData.data.features[0].attributes : null
				}
			});
		}
		catch(error){
			res.status(500).json({
				"success" : false,
				"message" : error.message,
			})
		}
	} else {
		res.status(204).send({});
	}
});
// NYC APIs END HERE

// WEB SCRAPING API START HERE
// web scraping
app.get("/getNYCProjectDetails", async (req, res) => {
	try{
		const url = `https://a810-bisweb.nyc.gov/bisweb/PropertyProfileOverviewServlet?bin=${req.query.bin}&requestid=1`
		try {
			const resData = await axios.get(url);
			const $= cheerio.load(resData.data);
			const BIN_No = $("table:nth-child(2) tr:nth-child(2) td:nth-child(3)").text();
			const Partial_Vacent_Order = $(".YellowBanner").text();
			const DOB_ECB_VIOLATION = $(".footnote a").text();
			const VIOLATION = $(".RedBanner").text();
			const Landmark = $("table:nth-child(5) tr:nth-child(6) td:nth-child(2)").text();
			const Environmental_Restrictions = $("table:nth-child(5) tr:nth-child(10) td:nth-child(2)").text();
			const Additional_Designations = Partial_Vacent_Order === '' && VIOLATION === '' ? $("table:nth-child(5) tr:nth-child(17) td:nth-child(2)").text() : $("table:nth-child(5) tr:nth-child(16) td:nth-child(2)").text();
			const Tidal_Wetlands = $("table:nth-child(7) tr:nth-child(4) td.content:nth-child(2)").text();
			const Fresh_Water_Wetlands = $("table:nth-child(7) tr:nth-child(5) td.content:nth-child(2)").text();
			const Coastal_Erosion_Hazard = $("table:nth-child(7) tr:nth-child(6) td.content:nth-child(2)").text();
			const Flood_Hazard_Zone = $("table:nth-child(7) tr:nth-child(7) td.content:nth-child(2)").text();

			res.status(200).json({
				"success" : true,
				"data" : { 
					BIN_No : BIN_No.replaceAll('BIN#', '').trim(), 
					Partial_Vacent_Order : Partial_Vacent_Order !== "" ? Partial_Vacent_Order : 'No',
					DOB_ECB_VIOLATION : DOB_ECB_VIOLATION !== "" ? DOB_ECB_VIOLATION.split('DOB').join('DOB, ').trim() : "No",
					VIOLATION : VIOLATION !== "" ? VIOLATION : 'No',
					Additional_Designations : Additional_Designations.replaceAll('\n\t\t\t', '').trim(), 
					Landmark : Landmark !== '' ? Landmark : "No", 
					Environmental_Restrictions, 
					Tidal_Wetlands : Tidal_Wetlands.length < 4 && Tidal_Wetlands !== '' ? Tidal_Wetlands : "No", 
					Fresh_Water_Wetlands : Fresh_Water_Wetlands.length < 4 && Fresh_Water_Wetlands !== '' ? Fresh_Water_Wetlands : "No", 
					Coastal_Erosion_Hazard : Coastal_Erosion_Hazard.length < 4 && Coastal_Erosion_Hazard !== '' ? Coastal_Erosion_Hazard : "No", 
					Flood_Hazard_Zone :Flood_Hazard_Zone.length < 4 &&Flood_Hazard_Zone !== '' ? Flood_Hazard_Zone : "No",
				}
			});

		} catch (error) {
			res.status(404).json({
				"success" : false,
				"message" : error.message
			})
		}
	}
	catch(error){
		res.status(404).json({ "success" : false, "message" : "error while scraping data"})
	}
});
// WEB SCRAPING API END HERE

app.post('/SendNotification', (req, res) => {
	console.log(req.body);
	let catalystApp = catalyst.initialize(req, { type: catalyst.type.applogic });
	let users = req.body.users;
	console.log(req.body);
	catalystApp.pushNotification()
		.web().sendNotification(req.body.message, users)
		.then((result) => {
			console.log(result);
			res.status(200).json("Success");
		})
		.catch((err) => {
			res.status(500).json(err);
		})
})

app.post('/workdrive', (req, res) => {
	let catalystApp = catalyst.initialize(req, { type: catalyst.type.applogic });
	let zcql = catalystApp.zcql();
	let jsonData = req.body;
	console.log(req.body);
	let config = {}

	if (req.body.config.Type == "Get" || req.body.config.Type == "Search") {
		config = {
			url: req.body.config.url,
			method: req.body.config.method,
			headers: req.body.config.headers
		};
		axios(config)
			.then(function (response) {
				res.status(200).json(response.data);
			})
			.catch(function (error) {
				res.status(500).json(error);
			});
	}
	else if (req.body.config.Type == "Create" || req.body.config.Type == "Copy" || req.body.config.Type == "Download") {
		config = {
			url: req.body.config.url,
			method: req.body.config.method,
			headers: req.body.config.headers
		};
		axios.post(config.url, { data: req.body.config.data }, { headers: config.headers })
			.then(function (response) {
				res.status(200).json(response.data);
			})
			.catch(function (error) {
				res.status(500).json(error);
			});

	}
	else if (req.body.config.Type == "Rename" || req.body.config.Type == "Delete" || req.body.config.Type == "Move") {
		config = {
			url: req.body.config.url,
			method: req.body.config.method,
			headers: req.body.config.headers
		};
		axios.patch(config.url, { data: req.body.config.data }, { headers: config.headers })
			.then(function (response) {
				res.status(200).json(response.data);
			})
			.catch(function (error) {
				res.status(500).json(error);
			});
	}
	else if (req.body.config.Type == "Upload") {
		config = {
			url: req.body.config.url,
			method: req.body.config.method,
			headers: req.body.config.headers,
		};

		try {
			let base64Image = req.body.config.data.content.split(';base64,').pop();
			var dir = '../' + req.body.config.data.filename;
			const imgBuffer = Buffer.from(base64Image, 'base64');
			var s = new Readable();
			s.push(imgBuffer);
			s.push(null);
			// dir = path.join(__dirname, dir);
			// fs.writeFileSync(dir, base64Image, { encoding: 'base64' });
			let formData = new FormData();
			let headers = formData.getHeaders();
			headers.Authorization = config.headers.Authorization;
			//let file = fs.createReadStream(req.body.config.data.filename);
			formData.append("content", s.pipe(fs.createWriteStream(req.body.config.data.filename)));
			formData.append("parent_id", req.body.config.data.parent_id);
			axios.post(config.url, formData, { headers: headers })
				.then(function (response) {
					fs.unlinkSync(dir);
					res.status(200).json("Success");

				})
				.catch(function (error) {
					res.status(500).json(error);
				});
		}
		catch (err) {
			res.status(500).json(err);
		}

	}


})

app.post('/task', (req, res) => {
	let catalystApp = catalyst.initialize(req, { type: catalyst.type.applogic });
	let zcql = catalystApp.zcql();
	let jsonData = req.body;
	console.log(req.body);
	let config = {}

	if (req.body.config.Type == "Get") {
		config = {
			url: req.body.config.url,
			method: req.body.config.method,
			headers: req.body.config.headers
		};
		axios.get(req.body.config.url, { headers: req.body.config.headers })
			.then(function (response) {
				res.status(200).json(response.data);
			})
			.catch(function (error) {
				res.status(500).json(error);
			});
	}
	else if (req.body.config.Type == "Create") {
		var data = new FormData();
		data.append(req.body.config.data.key, req.body.config.data.content);
		let headers = data.getHeaders()
		config = {
			method: 'post',
			url: req.body.config.url,
			headers: {
				...req.body.config.headers,
				...headers
			},
			data: data
		};

		axios(config)
			.then(function (response) {
				res.status(200).json(response.data);
			})
			.catch(function (error) {
				res.status(500).json(error);
			});

	}
	else if (req.body.config.Type == "FormData") {
		var data = new FormData();
		let keys = Object.keys(req.body.config.data);
		keys.forEach((key) => {
			data.append(key, req.body.config.data[key]);
		})
		let headers = data.getHeaders()
		config = {
			method: req.body.config.method,
			url: req.body.config.url,
			headers: {
				...req.body.config.headers,
				...headers
			},
			data: data
		};

		axios(config)
			.then(function (response) {
				res.status(200).json(response.data);
			})
			.catch(function (error) {
				res.status(500).json(error.message);
			});

	}
	else if (req.body.config.Type == "Update") {
		config = {
			url: req.body.config.url,
			method: req.body.config.method,
			headers: req.body.config.headers
		};
		axios.put(config.url, req.body.config.data, { headers: config.headers })
			.then(function (response) {
				res.status(200).json(response.data);
			})
			.catch(function (error) {
				res.status(500).json(error);
			});
	}
	else if (req.body.config.Type == "Remove") {
		config = {
			url: req.body.config.url,
			method: req.body.config.method,
			headers: req.body.config.headers
		};
		axios.delete(config.url, { headers: config.headers })
			.then(function (response) {
				res.status(200).json(response.data);
			})
			.catch(function (error) {
				res.status(500).json(error);
			});
	}
	else if (req.body.config.Type == "Upload") {
		config = {
			url: req.body.config.url,
			method: req.body.config.method,
			headers: req.body.config.headers,
		};

		try {
			let base64Image = req.body.config.data.content.split(';base64,').pop();
			var dir = '../' + req.body.config.data.filename;
			dir = path.join(__dirname, dir);
			fs.writeFileSync(dir, base64Image, { encoding: 'base64' });
			let formData = new FormData();
			let headers = formData.getHeaders();
			headers.Authorization = config.headers.Authorization;
			let file = fs.createReadStream(dir);
			formData.append("uploaddoc", file);
			// formData.append("parent_id", req?.config.data.parent_id);
			axios.post(config.url, formData, { headers: headers })
				.then(function (response) {
					fs.unlinkSync(dir);
					res.status(200).json(response.data);

				})
				.catch(function (error) {
					res.status(500).json(error);
				});
		}
		catch (err) {
			res.status(500).json(err);
		}

	}


})

app.post('/executeZohoFlow', async (req, res) => {
	let jsonData = req.body;
	console.log(req.body);
	let config = {}
	await axios.post(jsonData.config.url, {headers: {
        'Content-Type': 'application/json',
    },
	data: jsonData.config.data})
		.then(function (response) {
			res.status(200).json(response.data);
		})
		.catch(function (error) {
			res.status(500).json(error);
		});
})

app.get('/token/:token', (req, res) => {
	let catalystApp = catalyst.initialize(req, { type: catalyst.type.applogic });
	let zcql = catalystApp.zcql();
	let jsonData = req.body;
	console.log(req.body);
	let config = {}
	let url = "https://accounts.zoho.com/oauth/v2/token?refresh_token=" + req.params.token + "&client_secret=309ded77630e5c0966f8c91eb8270731bf913d4def&grant_type=refresh_token&client_id=1000.O5VWC2X0D2S5FP5H7F92YMMPXW7MEG"
	axios.post(url)
	  .then((result) => {
		result.data.tokenTime = new Date();
		res.status(200).json(result.data);
	  })
	  .catch((err) => {
		res.status(500).json(err);
	  })
})

app.post('/token_code', (req, res) => {
	let catalystApp = catalyst.initialize(req, { type: catalyst.type.applogic });
	let zcql = catalystApp.zcql();
	let jsonData = req.body;
	console.log(req.body);
	let config = {}
	let api = "https://accounts.zoho.com/oauth/v2/token?code=" + req.body.code + "&client_secret=309ded77630e5c0966f8c91eb8270731bf913d4def&redirect_uri=" + req.body.redirect_url + "&grant_type=authorization_code&client_id=1000.O5VWC2X0D2S5FP5H7F92YMMPXW7MEG";
	//let api="https://accounts.zoho.com/oauth/v2/token?code="+req?.code+"&client_secret=309ded77630e5c0966f8c91eb8270731bf913d4def&redirect_uri=http://localhost:3000/app/&grant_type=authorization_code&client_id=1000.O5VWC2X0D2S5FP5H7F92YMMPXW7MEG";
	axios.post(api)
	  .then((result) => {
		result.data.tokenTime = new Date();
		res.status(200).json(result.data);
	  })
	  .catch((err) => {
		res.status(500).json(err);
	  })
})

app.post('/chat', (req, res) => {
	let catalystApp = catalyst.initialize(req, { type: catalyst.type.applogic });
	let zcql = catalystApp.zcql();
	let jsonData = req.body;
	console.log(req.body);
	let config = {}

	if (req.body.config.Type == "Get") {
        config = {
          url: req.body.config.url,
          method: req.body.config.method,
          headers: req.body.config.headers
        };
        axios.get(req.body.config.url, { headers: req.body.config.headers })
          .then(function (response) {
			res.status(200).json(response.data);
          })
          .catch(function (error) {
			res.status(500).json(error);
          });
      }
      else if (req.body.config.Type == "Create") {
        config = {
          url: req.body.config.url,
          method: req.body.config.method,
          headers: req.body.config.headers
        };
        axios.post(config.url, req.body.config.data, { headers: config.headers })
          .then(function (response) {
			res.status(200).json(response.data);
          })
          .catch(function (error) {
			res.status(500).json(error);
          });

      }
      else if (req.body.config.Type == "Update") {
        config = {
          url: req.body.config.url,
          method: req.body.config.method,
          headers: req.body.config.headers
        };
        axios.put(config.url, req.body.config.data, { headers: config.headers })
          .then(function (response) {
			res.status(200).json(response.data);
          })
          .catch(function (error) {
			res.status(500).json(error);
          });
      }
      else if (req.body.config.Type == "Remove") {
        config = {
          url: req.body.config.url,
          method: req.body.config.method,
          headers: req.body.config.headers
        };
        axios.delete(config.url, { headers: config.headers })
          .then(function (response) {
            res.status(200).json(response.data);
          })
          .catch(function (error) {
            res.status(500).json(error);
          });
      }
      else if (req.body.config.Type == "Upload") {
        config = {
          url: req.body.config.url,
          method: req.body.config.method,
          headers: req.body.config.headers,
        };

        try {
          let base64Image = req.body.config.data.content.split(';base64,').pop();
          var dir = '../' + req.body.config.data.filename;
          dir = path.join(__dirname, dir);
          fs.writeFileSync(dir, base64Image, { encoding: 'base64' });
          let formData = new FormData();
          let headers = formData.getHeaders();
          headers.Authorization = config.headers.Authorization;
          let file = fs.createReadStream(dir);
          formData.append("uploaddoc", file);
          // formData.append("parent_id", req?.config.data.parent_id);
          axios.post(config.url, formData, { headers: headers })
            .then(function (response) {
              fs.unlinkSync(dir);
			  res.status(200).json(response.data);

            })
            .catch(function (error) {
				res.status(500).json(error);
            });
        }
        catch (err) {
			res.status(500).json(err);
        }

      }


})

app.put('/downloadECBViolation', (req, res) => {
	let catalystApp = catalyst.initialize(req, { type: catalyst.type.applogic });
	let zcql = catalystApp.zcql();
	let jsonData = req.body;
	console.log(req.body);
	let value =  req.body.value;
	var config1 = {
	  method: 'get',
	  encoding: null,
	  responseType: "arraybuffer",
	  responseEncoding: "binary",
	  url: 'http://a820-ecbticketfinder.nyc.gov/GetViolationImage?violationNumber=0' + value,
	  headers: {
		'Content-Type': 'application/pdf',
		// 'Cookie': '_abck=EE217ABB4090C2D333E26CE6CFE80458~-1~YAAQ5AVaaEjADiWBAQAAblsrKAfAvDwYQS2r7hVJj1odT7mXiOY8dC5QbGo4rOzWJdcP0i0RE5sOmVZiZ9J84GfS4g1DDH3pKEFgFP4Yq1HRjSW7F1Qug/a4ClvkFquVZH32khKYMlIcle5IOgu8QipD3Plf6ImcWSfEg0O2Fd79MpJ+kQ3R/yxPEMrEthDDp2y/HPtd8ldbtoYTvIYAzdJ9WI7UlMsLPKJdpJaRApYL7ropyCjd+wxj0SRDGcMcMCcnZ2LI6W00X1DZ58w02MdLftCDM/8VxTlcJx1cRc/+OH2g4gNkA6Hfxf0EA6HpkmEEkltZkGhBvnS84pbpMtI=~-1~-1~-1; bm_sz=83EDC8EC4DC952884BC65D9A3756A8AA~YAAQ5AVaaEnADiWBAQAAblsrKA+UO3x5MrouHArp6IkNP8py1GpNJ4m6QdkqZ3u5paK7ZwtqcgKdiolf+QYfF7zHkZKxXtYg0bWUvsSCRUgLBkerQ7SN/188RWzzYTRCDmp9GYLhZJDqZAepSJx1LxbwrAdI23nxKHTmtdQNpHuaKfuryXTyZwDsVOLfn7p23J/Cnhvhxi8+QKKcWH0grsGWwVKNugmPAaZrn4NxVnEnAEnXOBVwcpM7ucpzl8NSUkgtFxUuehaPj4ptgaL2lm1kZJpw20nqXn+iEb/x5bs=~3223618~4602163'
	  },

	};
	axios(config1)
	.then((result)=>{
	  result.data=result.data.toString('base64');
	  result.data="data:application/pdf;base64," +result.data
	  res.status(200).json(result.data);
	})
	.catch((err)=>{
		res.status(500).json(err);
	})
})

// new code 
// Author : @Hrushikesh Shelke 

app.get('/getAllCategories',(req,res)=>{
	try{
	let query="Select * from Categories;"
	let catalystApp = catalyst.initialize(req, { type: catalyst.type.applogic });
	let zcql = catalystApp.zcql();
	const zcqlPromise = zcql.executeZCQLQuery(query);
	zcqlPromise.then(queryResult => {
			let data=queryResult.map((d)=>{
				let ob={
					"_id" : d.Categories.ROWID,
					"CategoryName": d.Categories.CategoryName,
					"createdAt": d.Categories.CREATEDTIME,
					"updatedAt":d.Categories.MODIFIEDTIME,
					"__v":0
				}
				return ob;
				});
			res.status(200).json(data);
		})
	}catch(err){
		res.send(200).json({
			"success" : false,
			"data" : "Error While Fetching Categories",
		})
	}

})

app.get("/getPropertycomplaintData",async(req,res)=>{
	const urlComplaintRecieved = 'https://data.cityofnewyork.us/resource/eabe-havv.json?bin=' + req.query.bin;
    let rs = await axios.get(urlComplaintRecieved);
	let filterRes = rs.data.filter((one) => one.disposition_code == 'A3' || one.disposition_code == 'L1')
    filterRes.sort((a, b) => {
        if (a.disposition_code == 'A3') {
          return -1;
        }
        else if (a.disposition_code == 'L1') {
          return 1;
        } else {
          return 0;
        }
      })
	res.status(200).json(filterRes);
})

app.get("/getallpvmodule",(req, res) =>{
	try{
	let query="Select * from PvModules;"
	let catalystApp = catalyst.initialize(req, { type: catalyst.type.applogic });
	let zcql = catalystApp.zcql();
	const zcqlPromise = zcql.executeZCQLQuery(query);
	zcqlPromise.then(queryResult => {
			let data=queryResult.map((d)=>{
				let ob={ 
					    "_id" : d.PvModules.ROWID,
						"modelNumber" : d.PvModules.modelNumber,
						"manufacturer" : d.PvModules.manufacturer,
						"ulNumber" : d.PvModules.ulNumber,
						"builtinAC" : d.PvModules.builtinAC,
						"associatedmicroType" : d.PvModules.associatedmicroType,
						"cellQuantity" : d.PvModules.cellQuantity,
						"maxDCwatt" : d.PvModules.maxDCwatt,
						"mppVoltage" : d.PvModules.mppVoltage,
						"mppCurrent" : d.PvModules.mppCurrent,
						"openCircuitVolt" : d.PvModules.openCircuitVolt,
						"shortCircuitCurr" : d.PvModules.shortCircuitCurr,
						"cecEfficiency" : d.PvModules.cecEfficiency,
						"tempCoeffOfIsc" : d.PvModules.tempCoeffOfIsc,
						"tempCoeffOfPmpp" : d.PvModules.tempCoeffOfPmpp,
						"tempCoeffOfVoc" : d.PvModules.tempCoeffOfVoc,
						"maxSystemVolt" : d.PvModules.maxSystemVolt,
						"createdAt" : d.PvModules.CREATEDTIME,
						"updatedAt" :d.PvModules.MODIFIEDTIME	,
						"__v" : 0
				}
				
				return ob;
				});
			res.status(200).json(data);
		})
	}catch(err){
		res.send(200).json({
			"success" : false,
			"data" : "Error While Fetching Categories",
		})
	}
})

//    helper methods for create View starts--
async function createNewView(req){
	let catalystApp = catalyst.initialize(req, { type: catalyst.type.applogic });
	let zcql = catalystApp.zcql();
	let query= `INSERT INTO views_master (module, UserId, ViewName, IsOpen, IsCreatedByMe, IsPublic)`
			+ `VALUES ('${req.body.module}','${req.body.UserId}','${req.body.ViewName}',${req.body.IsOpen},${req.body.IsCreatedByMe},${req.body.IsPublic});`;
	const zcqlPromise = zcql.executeZCQLQuery(query);
	let responce=await zcqlPromise.then(d=>{
		return d;
	})
	return responce;
}
async function prepairArrayOfJsonColumns(cols){
	let arr=[];
	let temp=[];
	let count=0; 
	for(let i=0;i<cols.length;i++){
		if(temp.length!=35 && i!=(cols.length-1)){
			temp.push(cols[i]);
		}else if(temp.length==35 && i!=(cols.length-1)){
			arr.push(temp);
			temp=[];
			temp.push(cols[i]);
		}else if(temp.length!=35 && i==(cols.length-1)){
			temp.push(cols[i]);
			arr.push(temp);
		}
	}
	return arr;	
}
app.post("/testSplice",async (req,res)=>{
	let arr=await prepairArrayOfJsonColumns(req.body.str);
	res.send(arr);
})
async function insertSingleColArr(req,data,vId){
	try{
		let catalystApp = catalyst.initialize(req, { type: catalyst.type.applogic });
		let zcql = catalystApp.zcql();
		let cols=JSON.stringify(data);
		cols=cols.replaceAll("'","+");
		let query=`INSERT INTO view_column1 (view_id, columnJson) VALUES ('${vId}','${cols}');`;
		let zcqlPromise =await zcql.executeZCQLQuery(query);
		let k=await zcqlPromise.then(r=>{
			return r;
		})
	}catch(err){
		return err;
	}
	return k
}
async function insertViewColumnsNew(req,vId){
	let temp1=[];
	let arrofcols= await prepairArrayOfJsonColumns(req.body.columns);
	try{
		arrofcols.map(async v=>{
			let k=await insertSingleColArr(req,v,vId);
			temp1.push(k);
		})
	}catch(err){
		return "err"
	}
	return temp1;
}
//   create view helper methods end-----

app.post("/createView",async (req,res)=>{
	try{
		let cratedView=await createNewView(req);
		if(Object.keys(cratedView).length!=0){
			let viewId=cratedView[0].views_master.ROWID;
			let d=await insertViewColumnsNew(req,viewId);
			let responce=await getAllViews(req,viewId);
			let obj={
			"msg":"added successfully!",
			 	"data": responce[0],
			 }
			res.send(obj);
		}
		res.send("")
	}catch(error){
		res.send({"success" : false,"message" : error})
	}
	res.send("")
})

//   get view helper methods start---
function modifyGetViewData(rawData){
		let vId=0;
		let arr=[];
		let obj={};
	    let col=[];
        for(let i=0;i<rawData.length;i++){
            if(vId===0){
				vId=rawData[i].views_master.ROWID;
				obj={...rawData[i].views_master,
				"_id":rawData[i].views_master.ROWID,
				"createdAt":rawData[i].views_master.CREATEDTIME,
			    "updatedAt":rawData[i].views_master.MODIFIEDTIME
				}
				col=col.concat(JSON.parse(rawData[i].view_column1.columnJson.replaceAll("+","'")));
			}else if(rawData[i].views_master.ROWID==vId){
				 col=col.concat(JSON.parse(rawData[i].view_column1.columnJson.replaceAll("+","'")));
			}else if(rawData[i].views_master.ROWID!==vId && vId!==0){
				obj={...obj,"columns":col}
				arr.push(obj);
				obj={...rawData[i].views_master,
				"_id":rawData[i].views_master.ROWID,
				"createdAt":rawData[i].views_master.CREATEDTIME,
			    "updatedAt":rawData[i].views_master.MODIFIEDTIME
				}
				col=[];
				vId=rawData[i].views_master.ROWID;
                col=col.concat(JSON.parse(rawData[i].view_column1.columnJson.replaceAll("+","'")));
			}  
        }
        obj={...obj,"columns":col
        }
        arr.push(obj);
		return arr;
}
async function getAllViews(req,viewId){
	let catalystApp = catalyst.initialize(req, { type: catalyst.type.applogic });
	let zcql = catalystApp.zcql();
	if(viewId===0){
		let query='Select views_master.*,view_column1.* from view_column1 INNER JOIN views_master on views_master.ROWID = view_column1.view_id order by view_column1.view_id ;'
		const zcqlPromise = zcql.executeZCQLQuery(query);
		let rawData=await zcqlPromise.then(d=>{
			return d;
		})
	   let allViews= await modifyGetViewData(rawData)
	   return allViews;
	}else{
		let query=`Select views_master.*,view_column1.* from view_column1 INNER JOIN views_master on views_master.ROWID = view_column1.view_id where view_column1.view_id =${viewId} order by view_column1.view_id ;`
		const zcqlPromise = zcql.executeZCQLQuery(query);
		let rawData=await zcqlPromise.then(d=>{
			return d;
		})
		let allViews= await modifyGetViewData(rawData)
	    return allViews;
	}
}
async function modifyGetViewNew(rawData){
	let res=[];
	for(let i=0;i<rawData.length;i++){
		let cols=JSON.parse(rawData[i].view_column1.columnJson);
		let obj={
			...rawData[i].views_master,
			"columns":cols,
			"_id":rawData[i].views_master.ROWID,
			"createdAt":rawData[i].views_master.CREATEDTIME,
			"updatedAt":rawData[i].views_master.MODIFIEDTIME
		}
		res.push(obj);
	}
	return res;
}
//   get view helper methods ends---

/*
    info : Api to get All view and respective columns along with Public & created by me Views.
*/
app.get('/getViews',async (req,res)=>{
	let responce=await getAllViews(req,0);
	res.send(responce);
})
//   update view helper methods start--
async function isViewExist(vId,req){
	let catalystApp = catalyst.initialize(req, { type: catalyst.type.applogic });
	let zcql = catalystApp.zcql();
	let query=`Select views_master.ROWID from views_master where ROWID='${vId}';`;
	const zcqlPromise = zcql.executeZCQLQuery(query);
    let isPresent=await zcqlPromise.then(r=>{
		if(r.length>0){
			return true
		}else{
			return false
		}
	})
	return isPresent;
}
async function deleteViewColumnsByViewId(req,flag){
	let isDeleted;
	try{
		let catalystApp = catalyst.initialize(req, { type: catalyst.type.applogic });
		let zcql = catalystApp.zcql();
		let vId=flag==1?req.query.ROWID:req.body.ROWID;
		let query=`Delete view_column1 where view_id='${vId}';`;
		const zcqlPromise = zcql.executeZCQLQuery(query);
		isDeleted=await zcqlPromise.then(r=>{
			return r!=0;
		});
	}catch(err){

	}
	return isDeleted;
}
//   update view helper methods end--

/*
    info : Api to Update the view and also Update the column data of that view.
*/
app.post('/updateView',async (req,res)=>{
	try{
		let isDeleted=await deleteViewColumnsByViewId(req,0);
		let vId=req.body.ROWID;
		let columnsInserted;
		if(req.body.columns.length>0){
			columnsInserted=await insertViewColumnsNew(req,vId);
		}
		res.send({"columns_Updated":columnsInserted.length});
		
	}catch(err){
		res.send({"success" : false,"message" : "Error While Updating View",})
	}
})

/*
    info : Api to delete the view and also clear the column data of that view.
*/
app.delete('/deleteView',async (req,res)=>{
	try{
		let vId=req.query.ROWID;
		let isColDeleted=await deleteViewColumnsByViewId(req,1);
		if(isColDeleted){
			let catalystApp = catalyst.initialize(req, { type: catalyst.type.applogic });
			let zcql = catalystApp.zcql();
			let query=`Delete views_master where ROWID='${vId}';`;
			const zcqlPromise = zcql.executeZCQLQuery(query);
			let isDeleted=await zcqlPromise.then(r=>{
				return r!=0;
			});
			if(isDeleted){
				res.send({"success" : true,"message" : "View : "+vId+" Deleted!",})
			}
		}
	}catch(err){
		res.send(200).json({"success" : false,"message" : "Error While Deleting View",})
	}
});

//   task geild helper methods start---
async function createTaskFeilds(req){
	let data;
	try{
		let catalystApp = catalyst.initialize(req, { type: catalyst.type.applogic });
		let zcql = catalystApp.zcql();
		let query=`INSERT INTO task_master (DealId,UserName,UserId,Type,Lable,Name,DecimalPlaces,Value)`
		+` VALUES('${req.body.DealId?req.body.DealId:""}','${req.body.UserName?req.body.UserName:""}',`
		+`'${req.body.UserId?req.body.UserId:""}','${req.body.Type?req.body.Type:""}','${req.body.Lable?req.body.Lable:""}',`
		+`'${req.body.Name?req.body.Name:""}','${req.body.Config?req.body.Config.DecimalPlaces:''}','${req.body.Value?req.body.Value.Value:''}');`;
		const zcqlPromise = zcql.executeZCQLQuery(query);
		data=await zcqlPromise.then(r=>{
			return r;
		});
	}catch(err){
		
	}
	return data;
}
async function updateTaskFeilds(req){
	let data;
	try{
		let catalystApp = catalyst.initialize(req, { type: catalyst.type.applogic });
		let zcql = catalystApp.zcql();
		let query= ` UPDATE task_master SET `
					+` DealId = '${req.body.DealId?req.body.DealId:""}'`
					+` ,UserName = '${req.body.UserName?req.body.UserName:""}'`
					+` ,UserId = '${req.body.UserId?req.body.UserId:""}'`
					+` ,Type = '${req.body.Type?req.body.Type:""}'`
					+` ,Lable = '${req.body.Lable?req.body.Lable:""}'`
					+` ,Name = '${req.body.Name?req.body.Name:""}'`
					+` ,DecimalPlaces = '${req.body.Config?req.body.Config.DecimalPlaces:''}'`
					+` ,Value = '${req.body.Value?req.body.Value.Value:''}' where ROWID='${req.body.ROWID?req.body.ROWID:""}';`;
		const zcqlPromise = zcql.executeZCQLQuery(query);
		data=await zcqlPromise.then(r=>{
			return r;
		});
	}catch(err){
		
	}
	return data;
}
async function modifyMultipleTaskFeildData(rawData){
	let obj=[];
	for(let i=0;i<rawData.length;i++){
		let temp=await modifySingleTaskFeildData(rawData[i].task_master);
		obj.push(temp);
	}
	return obj;
}
async function modifySingleTaskFeildData(rawData){
	let obj={
		...rawData,
		"_id": rawData.ROWID,
		"Assign": rawData.DealId!=""?[{"DealId":rawData.DealId}]:[],
		"Value": rawData.Value!=""?{"Value":rawData.Value}:{},
		"Config":rawData.DecimalPlaces!=""?{"DecimalPlaces":rawData.DecimalPlaces}:{},
		"createdAt":rawData.CREATEDTIME,
		"updatedAt":rawData.MODIFIEDTIME
	}
	return obj;
}
async function getAllTaskFields(req){
	let rawData;
	try{
		let catalystApp = catalyst.initialize(req, { type: catalyst.type.applogic });
		let zcql = catalystApp.zcql();
		let query=`Select * from task_master;`;
		const zcqlPromise = zcql.executeZCQLQuery(query);
		rawData=await zcqlPromise.then(r=>{
			return r;
		});
	}catch(err){

	}
	return rawData;
}
//   task geild helper methods end---

/*
    info : Api to create task-feilds.
*/
app.post("/createTaskField",async (req,res)=>{
	try{
		let rawData=await createTaskFeilds(req);
		if(Object.keys(rawData).length>0){
			let modifiedData= await modifySingleTaskFeildData(rawData[0].task_master);
			let obj={"Data":modifiedData,"Message":"Successfully Added."}
			res.send(obj);
		}else{
			let obj={"Data":[],"Message":"Data Not Added."}
			res.send(obj);
		}
	}catch(err){
		res.send({"success" : false,"message" : err})
	}
})

/*
    info : Api to get all task-feilds.
*/
app.get("/getTaskFeilds",async (req,res)=>{
	try{
		let rawData=await getAllTaskFields(req);
		let modifiedData =await modifyMultipleTaskFeildData(rawData);
		let obj={
			"Data":modifiedData,
		}
		res.send(obj);
	}catch(err){
		res.send({"success" : false,"message" : err})
	}
})

/*
    info : Api to update task-feilds.
*/
app.post("/updateTaskField",async (req,res)=>{
	try{
		let rawData=await updateTaskFeilds(req);
		if(Object.keys(rawData).length>0){
			let modifiedData= await modifySingleTaskFeildData(rawData[0].task_master);
			let obj={"Data":modifiedData,"Message":"Successfully Added."}
			res.send(obj);
		}else{
			let obj={"Data":[],"Message":"Data Not Added."}
			res.send(obj);
		}
	}catch(err){
		res.send({"success" : false,"message" : err});
	}
})

/*
    info : Api to delete task-feilds.
*/
app.delete("/deleteTaskField",async (req,res)=>{
	try{
		let catalystApp = catalyst.initialize(req, { type: catalyst.type.applogic });
		let zcql = catalystApp.zcql();
		let query=`Delete task_master where ROWID = '${req.body.id}';`;
		const zcqlPromise = zcql.executeZCQLQuery(query);
		let data = await zcqlPromise.then(r=>{
			return r;
		});
		res.send({"msg":"Deleted Successfull"});
	}catch(err){
		res.send({"success" : false,"message" : err})
	}
})
module.exports = app;

